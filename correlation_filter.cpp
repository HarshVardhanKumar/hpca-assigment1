#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters) {

    int i9 = FILTER_SIZE + 1,h_w;
    for(int i = 0; i < filters; ++i) { 
    	i9 = i9 + i*FILTER_SIZE*FILTER_SIZE ;
     	int * copy = new int [height*width];   
        
	for(int h = 0; h<height; ++h) {        
		h_w = h*width;
            	
		//for(int start = 0; start<width; start+=8) {
		for(int w = 0; w<width; ++w) {
                	copy[h_w + w] = 0;
                	
			for(int offy = -1; offy <= 1; ++offy) {
                  		// for offx = 0;
                        	if(offy + w < 0 || offy + w >= width)
                            	continue;
                        	copy[h_w + w] += image[h_w + offy + w] * filter[i9 +offy];
                	}
            	}
	    	if(h+1<height)
		for(int w = 0; w<width; ++w){
			for(int offy = -1; offy <=1; ++offy) {
				if(offy+w<0|| offy+w>=width) continue;
				copy[h_w+w]+= image [h_w+width + offy+w]*filter[i9+FILTER_SIZE+offy];
			}
		}
	    	if(h-1>=0)
		for(int w = 0; w<width; ++w){
			for(int offy = -1; offy <=1; ++offy) {
				if(offy+w<0 || offy+w>=width) continue;
				copy[h_w+w] += image[h_w-width + offy+w]*filter[i9-FILTER_SIZE+offy];
			}
		}
		}
        //}
        int hh = 0;
	for(int h = 0; h<height; h++)
        {	hh = h*width;
            for(int w=0; w<width; w++)
                image[hh+ w] = copy[hh + w];
        }
        delete copy;
    }
}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}